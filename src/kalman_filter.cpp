#include "kalman_filter.h"
#include "tools.h"
using Eigen::MatrixXd;
using Eigen::VectorXd;
#include<iostream>

using namespace std;

// Please note that the Eigen library does not initialize 
// VectorXd or MatrixXd objects with zeros upon creation.

KalmanFilter::KalmanFilter() {}

KalmanFilter::~KalmanFilter() {}

void KalmanFilter::Init(VectorXd &x_in, MatrixXd &P_in, MatrixXd &F_in,
                        MatrixXd &H_in, MatrixXd &R_in, MatrixXd &Q_in) {
  x_ = x_in;
  P_ = P_in;
  F_ = F_in;
  H_ = H_in;
  R_ = R_in;
  Q_ = Q_in;
}

void KalmanFilter::Predict() {


  x_ = F_*x_;
  P_ = F_*P_*F_.transpose()+Q_;
  /**
  TODO:
    * predict the state
  */

}

void KalmanFilter::Update(const VectorXd &z) {

cout<<"In update"<<endl;
  VectorXd y;
  y = z - H_ * x_;
  cout<<"1"<<endl;
  MatrixXd Ht = H_.transpose();
  cout<<"2"<<endl;
	MatrixXd S = H_ * P_ * Ht + R_;
  cout<<"3"<<endl;
	MatrixXd Si = S.inverse();
  cout<<"4"<<endl;
	MatrixXd PHt = P_ * Ht;
cout<<"5"<<endl;  
	MatrixXd K = PHt * Si;
  cout<<"K gain"<<endl;
  x_ = x_ + (K*y);
  long x_size = x_.size();
	MatrixXd I = MatrixXd::Identity(x_size, x_size);
	P_ = (I - K * H_) * P_;
cout<<"Done Update"<<endl;
  /**
  TODO:
    * update the state by using Kalman Filter equations
  */
}

void KalmanFilter::UpdateEKF(const VectorXd &z) {


cout<<"Extended Update"<<endl;
  Tools tool ;

  MatrixXd Hjacob = tool.CalculateJacobian(x_);
  cout<<Hjacob<<endl;
  cout<<"Calculate Jacobian"<<endl;
  VectorXd polar_x = tool.CartesianPolar(x_);
  cout<<"polar"<<endl;
  VectorXd y = z - polar_x;
  cout<<"1"<<endl;
  MatrixXd Ht = Hjacob.transpose();
  cout<<"2"<<endl;
	MatrixXd S = Hjacob * P_ * Ht + R_;
  cout<<"3"<<endl;
	MatrixXd Si = S.inverse();
  cout<<"4"<<endl;
	MatrixXd PHt = P_ * Ht;
cout<<"5"<<endl;  
	MatrixXd K = PHt * Si;
  cout<<"K gain"<<endl;
  y(1) = atan2(sin(y(1)),cos(y(1)));
  x_ = x_ + (K*y);
  long x_size = x_.size();
	MatrixXd I = MatrixXd::Identity(x_size, x_size);
	P_ = (I - K * Hjacob) * P_;
  /**
  TODO:
    * update the state by using Extended Kalman Filter equations
  */
  cout<<"Done Extended Update"<<endl;
}
