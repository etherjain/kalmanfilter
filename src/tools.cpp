#include <iostream>
#include "tools.h"

using Eigen::VectorXd;
using Eigen::MatrixXd;
using std::vector;

Tools::Tools() {}

Tools::~Tools() {}

VectorXd Tools::CalculateRMSE(const vector<VectorXd> &estimations,
                              const vector<VectorXd> &ground_truth) {
  
  VectorXd RMSE(4);
  RMSE << 0,0,0,0;

  if(estimations.size() != 0 && estimations.size() == ground_truth.size())
  {
    for(int i=0;i<estimations.size();i++)
    {
      VectorXd temp = estimations[i] - ground_truth[i];
      RMSE = RMSE.array()+temp.array()*temp.array();
    }
    RMSE = RMSE.array()/estimations.size();
    RMSE = RMSE.array().sqrt();
    cout<<"done"<<endl;
    return RMSE;
  }
  else
  {
    cout<<"error in rmse"<<endl;
    return RMSE;
  }
  /**
  TODO:
    * Calculate the RMSE here.
  */
}

VectorXd Tools::CartesianPolar(const VectorXd& v){

  const double THRESH = 0.0001;
  VectorXd polar_vec(3);

  const double px = v(0);
  const double py = v(1);
  const double vx = v(2);
  const double vy = v(3);

  const double rho = sqrt( px * px + py * py);
  const double phi = atan2(py, px); //accounts for atan2(0, 0)
  const double drho = (rho > THRESH) ? ( px * vx + py * vy ) / rho : 0.0;

  polar_vec << rho, phi, drho;
  return polar_vec;
}
MatrixXd Tools::CalculateJacobian(const VectorXd& x_state) {

  MatrixXd Hj(3,4);
	//recover state parameters
	float px = x_state(0);
	float py = x_state(1);
	float vx = x_state(2);
	float vy = x_state(3);

	//TODO: YOUR CODE HERE 
    if(px == 0 && py ==0)
    {
        for(int i=0;i<3;i++)
        {
            for(int j=0;j<4;j++)
            {
                Hj(i,j) =0;
            }
        }
    }
    else
    {
        Hj(0,0) = px/sqrt(pow(px,2)+pow(py,2));
        Hj(0,1) = py/sqrt(pow(px,2)+pow(py,2));
        Hj(0,2) = 0;
        Hj(0,3) = 0;
        Hj(1,0) = -py/(pow(px,2)+pow(py,2));
        Hj(1,1) = px/(pow(px,2)+pow(py,2));
        Hj(1,2) = 0;
        Hj(1,3) = 0;
        Hj(2,0) = py*(vx*py-vy*px)/pow(sqrt(pow(px,2)+pow(py,2)),3);
        Hj(2,1) = px*(vx*py-vy*px)/pow(sqrt(pow(px,2)+pow(py,2)),3);
        Hj(2,2) = px/sqrt(pow(px,2)+pow(py,2));
        Hj(2,3) = py/sqrt(pow(px,2)+pow(py,2));
    }
	//check division by zero
	
	//compute the Jacobian matrix

	return Hj;
  /**
  TODO:
    * Calculate a Jacobian here.
  */
}
